Úloha:
----------------------------------------------
`Oboznámenie sa s reťazcami a smerníkamiv C resp. v C++`

Napíšte program v jazyku C, ktorý bude používať menu s dole uvedenými možnosťami týkajúcimi sa významu slov v danom slovníku. Užívateľ zadá skupinu alfabetických znakov, ktoré sa budú hľadať v danom slovníku a tiež aj polohu daných znakov a to buď na začiatku slova, alebo uprostred slova, alebo na konci slova. Pre každý taký dotaz, Váš program musí najsť všetky slová splňujúce danú podmienku.



VSTUP
-----
Vstup do programu sa skladá z dvoch častí: zo slovníka, ktorý sa načíta zo súboru a zo série vstupov z klávesnice.

- `1. Slovník sa nachádza v textovom súbore `slovnik.txt`. Každý jeho riadok začína slovom v stĺpci 1 za ktorým následuje bodkočiarka a význam slova. Niekoľko prázdnych znakov sa môže nachádzať medzi bodkočiarkou a významom slova.`

- `2. Dotazy budú zadávané interaktívne z klávesnice a ich formát je určený následovným menu:`

~~~
    <starting>    ......... najdi význam slova začínajúceho
                             danými znakmi
    <containing> .......... najdi význam slova obsahujúceho
                             dané znaky
    <ending>      ......... najdi význam slova končiaceho
                             danými znakmi
    <stop>       .......... ukonči daný program.
~~~

V každom jednom dotaze uživateľ zadá skupinu znakov, ktoré sa budú hľadať v danom slovníku.

VÝSTUP
------

Program bude zapisovať výstup na monitor a tiež do textového súboru `out.txt`. Celá interakcia s programom bude zápísaná do tohto súboru. Pre každý dotaz sú možné tri rôzne výstupy:
- `1. Ak žiadne slovo v slovníku nesplňa daný dotaz, vypíšte príslušné hlásenie o tom.`
- `2. Ak iba jedno slovo spľňa daný dotaz, potom vypíšte toto slovo s jeho významom zo slovníka ako výstup.`
- `3. Ak viac ako jedno slovo spľňa daný dotaz, potom vypíšte zoznam všetkých takýchto slov bez uvedenia ich významu.`

DÁTOVÉ ŠTRUKTÚRY
----------------

Tento program musí špecifikovať údajový typ reťazec a využívať jeho vlastnosti spolu s príslušnými operáciami jazyku v C popr. v C++. Môžete predpokladať, že reťazec má dĺžku najviac 30 znakov a že príslušný slovník obsahuje najviac 30 rôznych slov spolu s ich významom.


ODOVZAJTE
---------

- `1. zdrojový kód programu`
- `2. vykonateľný kód programu`
- `3. dátový súbor: slovnik.txt a out.txt vytlačený na papieri`
- `4. osobná prezentácia na cvičení`
