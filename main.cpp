// main.cpp - Implementation of the Search class
// Date: 2018-03-04
// Created by: Erik Laco

#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <iomanip>     //For formatting output
using namespace std;

class Search
{
	public:
		string option;
		string input_string;
		vector<string> found_word;
		vector<string> found_word_description;

	public:
		int read_option()
		{
			cout << "Set option: ";
			cin >> this->option;
			return 0;
		}

	public:
		int read_string()
		{
			cout << "Search string: ";
			cin >> this->input_string;
			return 0;
		}

	private:
		int match_starting( string line )
		{
			size_t pos = line.find(";");				// Set char to split line
			string word = line.substr(0, pos);			// Save line from 0 position to found ; @pos
			string description = line.substr( pos+1 ); 	// Save description to variable

			if ( word.find( this->input_string ) == 0 ){
				this->found_word.push_back( word );
				this->found_word_description.push_back( description );
			}

			return 0;
		}

	private:
		int match_ending( string line )
		{
			size_t pos = line.find(";");				// Set char to split line
			string word = line.substr(0, pos);			// Save line from 0 position to found ; @pos
			string description = line.substr( pos+1 ); 	// Save description to variable

			if ( word.find( this->input_string ) == ( word.length() - this->input_string.length() ) ){
				this->found_word.push_back( word );
				this->found_word_description.push_back( description );
			}

			return 0;
		}

	private:
		int match_containing( string line )
		{
			size_t pos = line.find(";");					// Set char to split line
			string word = line.substr(0, pos );				// Save line from 0 position to found ;
			string description = line.substr( pos+1 ); 		// Save description to variable

			if ( word.find( this->input_string ) < word.length() )
			{
				this->found_word.push_back( word );
				this->found_word_description.push_back( description );
			}

			return 0;
		}

	public:
		int read_file()
		{
			string line;
			ifstream myfile( "slovnik.txt" );			// Open file
			if ( myfile.is_open() )						// Condition if file is successfully open
			{

				while ( getline(myfile, line) )			// Read file by lines
				{
					if ( this->option == "<starting>" )
					{
						match_starting( line );
					}
					else if ( this->option == "<ending>" )
					{
						// This function you change when you will have function for ending search
						match_ending( line );
					}
					else if ( this->option == "<containing>" )
					{
						// This function you change when you will have function for containing search
						match_containing( line );
					}
					else if ( this->option == "<stop>" )
					{
						cout << "Bye-Bye !" << endl;
						return 500;
					}
					else
					{
						cout << "Wrong option !!! Try again later..." << endl;
						break;
					}
				}

				myfile.close();							// Close file
			}
			else
			{
				cout << "Unable to open file\n";
				return 500;
			}

			return 0;
		}
};

int main()
{
	while ( true )
	{
		Search search;

		if ( search.read_option() == 0 && search.read_string() == 0 )
		{

			if ( search.read_file() == 0 )
			{
				cout << endl << "Found words:" << endl;
				for ( int i = 0; i < search.found_word.size(); i++ )
				{
					cout << search.found_word[ i ] << setw( 20 - search.found_word[ i ].length() ) << "->" << search.found_word_description[ i ] << endl;
				}
				cout << endl;
			}
			else
			{
				break;
			}

		}
	}

	return 0;
}
